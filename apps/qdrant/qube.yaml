qdrant:
  qubeKey: qdrant
  name: Qdrant
  version: 0.9.1
  chart: qdrant
  repo: https://qdrant.to/helm
  type: chart-helm-repository
  id: qdrant
  maturity: beta

  ui:
    icon: '<svg width="256px" height="296px" viewBox="0 0 256 296" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid">
          <title>qdrant</title><defs><linearGradient x1="81.5619048%" y1="44.8421053%" x2="-18.0857143%" y2="44.8421053%" id="linearGradient-1">
          <stop stop-color="#FF3364" offset="0%"></stop><stop stop-color="#C91540" stop-opacity="0" offset="100%"></stop></linearGradient></defs><g>
          <polygon fill="#24386C" points="201.31705 271.722427 195.422715 109.21267 184.747781 66.3682368 256 73.9112545 256 270.492509 212.474757 295.612626"></polygon>
          <polygon fill="#7589BE" points="255.995151 73.8998107 212.469908 99.037384 122.649634 79.3346471 17.5160008 122.140288 1.1370484e-14 73.8998107 63.9883137 36.9499053 127.996024 0 191.986277 36.9499053"></polygon>
          <polyline fill="#B2BFE8" points="0.00310340412 73.8998107 43.5283462 99.037384 68.7590217 174.073816 153.949405 242.236209 128.001067 295.599243 63.9933568 258.647398 0.00310340412 221.697492 0.00310340412 73.897871"></polyline>
          <polyline fill="#24386C" points="156.856906 202.807459 128.001067 245.347371 128.001067 295.603122 168.946605 271.978458 190.043934 240.475027"></polyline>
          <polygon fill="#7589BE" points="128.018523 195.107138 87.0555287 124.184656 95.8787005 100.678309 129.42068 84.4156955 168.946411 124.185819"></polygon>
          <polyline fill="#B2BFE8" points="87.0555287 124.178837 128.001067 147.803501 128.001067 195.091621 90.131778 196.720927 67.2247763 167.471344 87.0555287 124.178856"></polyline>
          <polygon fill="#24386C" points="128.001067 147.799621 168.946605 124.176897 196.813234 170.576668 163.090869 198.439418 128.001067 195.089293"></polygon>
          <path d="M168.946605,271.974579 L212.471848,295.601182 L212.471848,99.0393237 L170.226759,74.658205 L128.001067,50.2770864 L85.7559782,74.658205 L43.5302858,99.0393237 L43.5302858,196.581255 L85.7559782,220.962373 L128.001067,245.345432 L168.946605,221.699432 L168.946605,271.974579 Z M168.946605,171.443681 L128.001067,195.087742 L87.0555287,171.443681 L87.0555287,124.174957 L128.001067,100.530897 L168.946605,124.174957 L168.946605,171.443681" fill="#DC244C"></path>
          <polygon fill="url(#linearGradient-1)" points="128.018523 245.362888 128.018523 195.099379 87.2863443 171.657041 87.2863443 221.837146"></polygon></g></svg>'
    shortDescription: Vector Database
    description: Qdrant “is a vector similarity search engine that provides a production-ready service with a convenient API to store, search, and manage points (i.e. vectors) with an additional payload.” You can think of the payloads as additional pieces of information that can help you hone in on your search and also receive useful information that you can give to your users.
    links: 
    - linkName: Homepage
      url: https://qdrant.tech/
    - linkName: Documentation
      url: https://qdrant.tech/documentation/
    - linkName: API Docs
      url: https://qdrant.github.io/qdrant/redoc/index.html
    - linkName: Github
      url: https://github.com/qdrant/qdrant-helm/tree/main
    - linkName: Chart Source
      url: https://qdrant.to/helm
    catalogItem: true
    maxNumOfInstances: 1
    cpu:
    ram:
    disk:

  metadata:
    id: qdrant
    ingress: "{{ metadata.id }}.{{ defaults.workspace.subdomain }}.{{ defaults.workspace.domain }}"
    url: "{{ metadata.ingress }}/dashboard"

  properties: {}

  schemaType: {}

  schemaData: {}

  extraParameters:
  - key: readinessProbe.enabled
    value: 'True'
    description: Defines whether the readiness probe is enabled.
  - key: readinessProbe.failureThreshold
    value: '6'
    description: The number of consecutive failures for the probe to be considered
      failed after having succeeded.
  - key: readinessProbe.initialDelaySeconds
    value: '5'
    description: The number of seconds after the container has started before the
      probe is initiated.
  - key: readinessProbe.periodSeconds
    value: '5'
    description: How often (in seconds) to perform the probe.
  - key: readinessProbe.successThreshold
    value: '1'
    description: The number of consecutive successes for the probe to be considered
      successful after having failed.
  - key: readinessProbe.timeoutSeconds
    value: '1'
    description: The number of seconds after which the probe times out.
  - key: replicaCount
    value: '1'
    description: Number of replicas to deploy
  - key: imagePullSecrets
    value: '[]'
    description: A list of references to secrets in the same namespace to use for
      pulling any of the images in pods scheduled.
  - key: metrics.serviceMonitor.additionalLabels
    value: '{}'
    description: Optional labels to add to the service monitor
  - key: metrics.serviceMonitor.enabled
    value: 'false'
    description: Flag to enable or disable the service monitor
  - key: metrics.serviceMonitor.metricRelabelings
    value: '[]'
    description: List of metric relabeling configurations
  - key: metrics.serviceMonitor.relabelings
    value: '[]'
    description: List of relabeling configurations
  - key: metrics.serviceMonitor.scrapeInterval
    value: 30s
    description: The time interval between scrapes
  - key: metrics.serviceMonitor.scrapeTimeout
    value: 10s
    description: The time to wait before a scrape times out
  - key: metrics.serviceMonitor.targetPath
    value: /metrics
    description: The endpoint path to scrape for metrics
  - key: metrics.serviceMonitor.targetPort
    value: http
    description: The port on which to scrape metrics
  - key: podSecurityContext.fsGroup
    value: '3000'
    description: A special supplemental group that applies to all containers in the
      pod.
  - key: podSecurityContext.fsGroupChangePolicy
    value: Always
    description: Specifies when to change the fsGroup ownership of a volume.
  - key: args
    value: 'null'
    description: List of script arguments
  - key: additionalLabels
    value: '{}'
    description: Additional labels to apply
  - key: ingress.additionalLabels
    value: '{}'
    description: Additional labels to be added to the ingress resource.
  - key: ingress.annotations
    value: '{}'
    description: Annotations to be added to the ingress resource.
  - key: ingress.enabled
    value: 'False'
    description: Flag to enable or disable the ingress resource.
  - key: ingress.hosts[0].host
    value: example-domain.com
    description: The domain name for the ingress host.
  - key: ingress.hosts[0].paths[0].path
    value: /
    description: The path for the ingress rule.
  - key: ingress.hosts[0].paths[0].pathType
    value: Prefix
    description: Type of path matching, can be 'Prefix' or 'Exact'.
  - key: ingress.hosts[0].paths[0].servicePort
    value: '6333'
    description: The port number of the service to be exposed.
  - key: ingress.ingressClassName
    value: ''
    description: The class of the ingress resource.
  - key: ingress.tls
    value: '[]'
    description: TLS configuration for the ingress resource.
  - key: additionalVolumes
    value: '[]'
    description: A list of additional volumes. Default value is an empty list.
  - key: affinity
    value: '{}'
    description: Defines the constraints for scheduling the pods, such as node and
      pod affinities.
  - key: sidecarContainers
    value: '[]'
    description: A list of containers that are used as sidecars.
  - key: apiKey
    value: 'False'
    description: Description for apiKey parameter
  - key: config.cluster.consensus.tick_period_ms
    value: '100'
    description: The tick period in milliseconds for consensus.
  - key: config.cluster.enabled
    value: 'True'
    description: Flag to enable or disable the cluster.
  - key: config.cluster.p2p.port
    value: '6335'
    description: The port number for peer-to-peer communication.
  - key: service.additionalLabels
    value: '{}'
    description: Additional labels to be added to the service.
  - key: service.annotations
    value: '{}'
    description: Annotations to be added to the service.
  - key: service.loadBalancerIP
    value: ''
    description: The load balancer IP address to assign to the service.
  - key: service.ports[0].checksEnabled
    value: 'True'
    description: Whether health checks are enabled for the http port.
  - key: service.ports[0].name
    value: http
    description: Name of the port for HTTP traffic.
  - key: service.ports[0].port
    value: '6333'
    description: Port number for HTTP traffic.
  - key: service.ports[0].protocol
    value: TCP
    description: Protocol for the http port.
  - key: service.ports[0].targetPort
    value: '6333'
    description: Target port for HTTP traffic.
  - key: service.ports[1].checksEnabled
    value: 'False'
    description: Whether health checks are enabled for the grpc port.
  - key: service.ports[1].name
    value: grpc
    description: Name of the port for gRPC traffic.
  - key: service.ports[1].port
    value: '6334'
    description: Port number for gRPC traffic.
  - key: service.ports[1].protocol
    value: TCP
    description: Protocol for the grpc port.
  - key: service.ports[1].targetPort
    value: '6334'
    description: Target port for gRPC traffic.
  - key: service.ports[2].checksEnabled
    value: 'False'
    description: Whether health checks are enabled for the p2p port.
  - key: service.ports[2].name
    value: p2p
    description: Name of the port for peer-to-peer traffic.
  - key: service.ports[2].port
    value: '6335'
    description: Port number for peer-to-peer traffic.
  - key: service.ports[2].protocol
    value: TCP
    description: Protocol for the p2p port.
  - key: service.ports[2].targetPort
    value: '6335'
    description: Target port for peer-to-peer traffic.
  - key: service.type
    value: ClusterIP
    description: Type of service.
  - key: fullnameOverride
    value: ''
    description: Override the fullname if provided
  - key: snapshotRestoration.enabled
    value: 'False'
    description: Indicates if snapshot restoration is enabled
  - key: snapshotRestoration.pvcName
    value: snapshots-pvc
    description: Name of the persistent volume claim used for snapshots
  - key: snapshotRestoration.snapshots
    value: 'null'
    description: Contains snapshot details, null indicates no snapshots are defined
  - key: podDisruptionBudget.enabled
    value: 'False'
    description: Indicates whether the Pod Disruption Budget is enabled.
  - key: podDisruptionBudget.maxUnavailable
    value: '1'
    description: Specifies the maximum number of unavailable pods.
  - key: podDisruptionBudget.unhealthyPodEvictionPolicy
    value: ''
    description: Defines the policy for evicting unhealthy pods.
  - key: podLabels
    value: '{}'
    description: Labels for the pod
  - key: nodeSelector
    value: '{}'
    description: A selector to match nodes for scheduling.
  - key: podAnnotations
    value: '{}'
    description: Annotations to be added to the pod, currently set as an empty dictionary.
  - key: additionalVolumeMounts
    value: '[]'
    description: A list of additional volume mounts.
  - key: tolerations
    value: '[]'
    description: Node tolerations for a pod. Tolerations are applied to pods and allow
      the pods to schedule onto nodes with matching taints.
  - key: serviceAccount.annotations
    value: '{}'
    description: Annotations for the service account.
  - key: podManagementPolicy
    value: Parallel
    description: Specifies how pods are created during initial scale up, update, or
      replacement.
  - key: startupProbe.enabled
    value: 'False'
    description: Indicates whether the startup probe is enabled.
  - key: startupProbe.failureThreshold
    value: '30'
    description: Minimum consecutive failures for the probe to be considered failed
      after having succeeded. The default value is 30.
  - key: startupProbe.initialDelaySeconds
    value: '10'
    description: Number of seconds after the container has started before liveness
      or readiness probes are initiated. The default value is 10.
  - key: startupProbe.periodSeconds
    value: '5'
    description: How often (in seconds) to perform the probe. The default value is
      5.
  - key: startupProbe.successThreshold
    value: '1'
    description: Minimum consecutive successes for the probe to be considered successful
      after having failed. The default value is 1.
  - key: startupProbe.timeoutSeconds
    value: '1'
    description: Number of seconds after which the probe times out. The default value
      is 1.
  - key: updateVolumeFsOwnership
    value: 'True'
    description: Update the ownership of the filesystem volume if this parameter is
      set to true.
  - key: image.pullPolicy
    value: IfNotPresent
    description: Defines the image pull policy, which determines if and when the image
      will be pulled from the repository.
  - key: image.repository
    value: docker.io/qdrant/qdrant
    description: Specifies the repository where the image is hosted.
  - key: image.tag
    value: ''
    description: Specifies the tag of the image to use. An empty string can be used
      to default to a specific tag (e.g., 'latest').
  - key: image.useUnprivilegedImage
    value: 'False'
    description: Indicates whether to use an unprivileged image, which can provide
      additional security by running as a non-root user.
  - key: persistence.accessModes
    value: '["ReadWriteOnce"]'
    description: The access modes for the persistent storage. Possible values typically
      include ReadWriteOnce, ReadOnlyMany, and ReadWriteMany.
  - key: persistence.annotations
    value: '{}'
    description: Annotations for the persistent storage. These are key-value pairs
      used to store additional metadata.
  - key: persistence.size
    value: 10Gi
    description: The size of the persistent storage in gigabytes (Gi).
  - key: nameOverride
    value: ''
    description: Override the name of the application.
  - key: containerSecurityContext.allowPrivilegeEscalation
    value: 'False'
    description: AllowPrivilegeEscalation determines if a process can gain more privileges
      than its parent process.
  - key: containerSecurityContext.privileged
    value: 'False'
    description: Privileged determines if a container runs in privileged mode, which
      gives it enhanced permissions on the host machine.
  - key: containerSecurityContext.readOnlyRootFilesystem
    value: 'True'
    description: ReadOnlyRootFilesystem indicates whether the root filesystem of the
      container is writable or read-only.
  - key: containerSecurityContext.runAsGroup
    value: '2000'
    description: RunAsGroup specifies the primary group ID to run the container process.
  - key: containerSecurityContext.runAsNonRoot
    value: 'True'
    description: RunAsNonRoot specifies that the container must run as a non-root
      user.
  - key: containerSecurityContext.runAsUser
    value: '1000'
    description: RunAsUser specifies the user ID to run the container process.
  - key: livenessProbe.enabled
    value: 'False'
    description: Indicates whether the liveness probe is enabled.
  - key: livenessProbe.failureThreshold
    value: '6'
    description: Specifies the number of consecutive failures required to consider
      the probe failed.
  - key: livenessProbe.initialDelaySeconds
    value: '5'
    description: The number of seconds after the container has started before the
      probe is initiated.
  - key: livenessProbe.periodSeconds
    value: '5'
    description: Specifies how often (in seconds) to perform the probe.
  - key: livenessProbe.successThreshold
    value: '1'
    description: The number of consecutive successes required to consider the probe
      successful after it has failed.
  - key: livenessProbe.timeoutSeconds
    value: '1'
    description: The number of seconds after which the probe times out.
  - key: readOnlyApiKey
    value: 'False'
    description: Flag indicating if the API key is read-only.
  - key: priorityClassName
    value: ''
    description: Defines the priority class name for the workload. Defaults to an
      empty string if not set.
