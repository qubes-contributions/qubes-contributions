# Qubinets Contributions

## Overview

Welcome to Qubinets! Our mission is to provide a hassle-free data infrastructure, enabling developers like you to concentrate more on creating value through application code rather than infrastructure complexities. We aspire to be the leading platform that accelerates the adoption of Artificial Intelligence and Big Data technologies by providing robust, reusable building blocks from a global development community.

## Prerequisites

To get started with Qubinets, your application should meet the following requirements:

- **Containerization**: Your application must be containerized. For guidance on containerizing, visit [Docker's Getting Started Guide](https://docs.docker.com/get-started/02_our_app/).
- **Package Management**: Use Helm to manage packages on a Kubernetes cluster. Learn more about Helm [here](https://helm.sh/docs/intro/using_helm/).

Once these prerequisites are met, your application can be seamlessly deployed on Qubinets and any cloud platform.

## Installation Guide

To contribute to Qubinets, your application configuration files should be formatted in YAML and include the following main sections:

### General Section

This section includes the essential details of your application:

``` yaml
keycloak:
  qubeKey: keycloak
  name: Keycloak
  version: 18.4.0
  chart: keycloak
  repo: https://charts.bitnami.com/bitnami
  type: chart-helm-repository
  id: keycloak
  maturity: alpha
```

| Key name  | Description                                                                             | 
|-----------|-----------------------------------------------------------------------------------------|
| qubeKey   | A unique identifier for the application.                                                | 
| name      | The name displayed in the Qubinets inventory UI.                                        | 
| version   | Specifies the Helm chart version or the branch name if a Git repository is used.        | 
| chart     | The Helm chart name or path in the Git repository where the local Helm chart is stored. |
| repo      | The Helm or Git repository URL.                                                         |
| type      | Type of repository used (`chart-helm-repository` or `chart-git-repository`).            |
| id        | A unique ID matching the qubeKey.                                                       |
| maturity  | The development stage of the Qube (`alpha`, `beta`, `stable`, `official`).              |


### UI SECTION

The `ui`, which is used to store our icon, links, buttons and description. We also defined the max 
allowed numbers of this qube in this section.

```yaml
  ui:
    icon: keycloak
    shortDescription: Open Source Identity and Access Management
    description: An open source software product to allow single sign-on with identity and access management aimed at modern applications and services. As of March 2018 this WildFly community project is under the stewardship of Red Hat who use it as the upstream project for their RH-SSO product.
    links:
    - linkName: Homepage
      url: https://www.keycloak.org/
    - linkName: Documentation
      url: https://www.keycloak.org/documentation
    - linkName: Github
      url: https://github.com/keycloak/keycloak
    catalogItem: true
    maxNumOfInstances: 1
    cpu: 1
    ram: 2
    disk: 10
```

| Key name          | Description                                                                                                                                                                                                            | 
|-------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| icon              | Image on the Qube should be provided as svg image, some images are stored in frontend and can <br/> be referenced by name. If no value is provided the first letter of the application will be provided as Qube image. | 
| shortDescription  | In this section user can provide short description of the application.                                                                                                                                                 | 
| description       | In this section longer description of the application functionality can be provided.                                                                                                                                   | 
| links             | Links is the list of the links related to application, like homepage, documentation, Git repo and other important links.                                                                                               |
| linkName          | Descriptive name of the application URL link                                                                                                                                                                           |
| url               | URL of the link.                                                                                                                                                                                                       |
| catalogItem       | Defines if application should be shown in Qubinets inventory UI or not.                                                                                                                                                |
| maxNumOfInstances | Maximum number of allowed qube to be installed in the same workspace.                                                                                                                                                  |
| cpu               | Defines expected CPU requirements for the application to run on Kubernetes cluster.                                                                                                                                    |
| ram               | Defines expected RAM memory requirements for the application to run on Kubernetes cluster.                                                                                                                             |
| disk              | Defines expected disk requirements for the application to run on Kubernetes cluster.                                                                                                                                   |


### VALUES.YAML SECTION

The third section is `values.yaml`. The values.yaml is used to define our configuration for this helm chart. Values.yaml
are stored as separate file in the repository.

```yaml
auth:
  adminUser: {{ properties.keycloakUsername.value }}
  adminPassword: "{{ properties.keycloakPassword.value }}"
tls:
  enabled: true
  autoGenerated: true
production: true
ingress:
  enabled: true
  ingressClassName: "nginx"
  hostname: {{ metadata.url }}
  servicePort: https
  annotations:
    nginx.ingress.kubernetes.io/backend-protocol: "HTTPS"
    nginx.ingress.kubernetes.io/proxy-buffering: "on"
    nginx.ingress.kubernetes.io/proxy-buffer-size: "16k"
  tls: true
  extraTls:
    - hosts:
        - "{{ metadata.url }}"
      secretName: wildcard-domain-tls
```

In values.yaml user can use templates from other sections like metadata, properties and schemaData.

### METADATA SECTION

The metadata are key:value pairs that you want to reuse later. The metadata values are inherited from Qubinets frontend
and only can be used in creating metadata key:value pairs.

```yaml
  metadata:
    id: keycloak
    domain: "{{ defaults.workspace.subdomain }}.{{ defaults.workspace.domain }}"
    url: "{{ metadata.id }}.{{ metadata.domain }}"
```

### PROPERTIES SECTION

Fields that you want to use internal in this qube like the login user/password. These parameters will be shown in Qubinets
UI as parameters that can be configured before the Qube is deployed on Kubernetes.

```yaml
  properties:
    keycloakUsername:
      type: text
      displayName: "Keycloak username"
      parameterId: keycloakUsername
      defaultValue: "admin"
      readOnly: false
      required: true
      validationRegex: "^[a-zA-Z][a-zA-Z0-9_@-]{2,15}$"
```

| Key name         | Description                                                                                                  | 
|------------------|--------------------------------------------------------------------------------------------------------------|
| keycloakUsername | Define a unique key for the parameter.                                                                       | 
| type             | Define a type of the parameter `text` or `password`. With `password` the content is masked.                  | 
| displayName      | Defines a name that will be displayed on Qubinets UI.                                                        | 
| parameterId      | Define a unique key for the parameter.                                                                       |
| defaultValue     | Defines a default value that will be set for this property if the user doesn't change it in the Qubinets UI. |
| readOnly         | Defines if the parameter can be changed or not by the user in Qubinets UI.                                   |
| required         | Defines if the field is required.                                                                            |
| validationRegex  | Defines the validation regex for the parameter that we can use to validate user input.                       |


### SCHEMATYPE SECTION

Fields that this qube want to expose to the outside, so other qubes wanting to connect to this one need to declare.

```yaml
  schemaType:
    logstash:
      kafka_topics:
        type: text
        displayName: "Kafka topic"
        defaultValue: "logstash"
        readOnly: false
        required: false
        validationRegex: "^[a-z0-9._-]{1,249}$"
```

| Key name        | Description                                                                                                  | 
|-----------------|--------------------------------------------------------------------------------------------------------------|
| logstash        | Define a Qube key for which this parameter is exposed.                                                       | 
| kafka_topics    | Define a name of the exposed parameter.                                                                      | 
| type            | Define a type of the parameter `text` or `password`. With `password` the content is masked.                  | 
| displayName     | Defines a name that will be displayed on Qubinets UI.                                                        |
| defaultValue    | Defines a default value that will be set for this property if the user doesn't change it in the Qubinets UI. |
| readOnly        | Defines if the parameter can be changed or not by the user in Qubinets UI.                                   |
| required        | Defines if the field is required.                                                                            |
| validationRegex | Defines the validation regex for the parameter that we can use to validate user input.                       |

### SCHEMADATA SECTION

They are fields that this qube need to declare to connect to other Qubes.

```yaml
  schemaData:
    oauthproxy:
      oauthurl:
        required: false
        readOnly: false
        defaultValue:
          loginUrl:
            type: text
            displayName: "Oauth URL"
            defaultValue: "login"
            readOnly: false
            required: true
            validationRegex: "^[a-z]{2,}$"
```

| Key name         | Description                                                                                                  | 
|------------------|--------------------------------------------------------------------------------------------------------------|
| oauthproxy       | Define a Qube key ofthat this Qube is connecting to.                                                         | 
| oauthurl         | Define a name of the schema that needs to be used.                                                           | 
| required         | Defines if the field is required.                                                                            | 
| readOnly         | Defines if the parameter can be changed or not by the user in Qubinets UI.                                   |
| defaultValue     | Defines a default value that will be set for this property if the user doesn't change it in the Qubinets UI. |
| loginUrl         | Define a name of the parameter that needs to be used.                                                        |
| type             | Define a type of the parameter `text` or `password`. With `password` the content is masked.                  |
| displayName      | Defines a name that will be displayed on Qubinets UI.                                                        |
| defaultValue     | Defines a default value that will be set for this property if the user doesn't change it in the Qubinets UI. |
| readOnly         | Defines if the parameter can be changed or not by the user in Qubinets UI.                                   |
| required         | Defines if the field is required.                                                                            |
| validationRegex  | Defines the validation regex for the parameter that we can use to validate user input.                       |
