{
  "sourceDefinitionId": "1fa90628-2b9e-11ed-a261-0242ac120002",
  "documentationUrl": "https://docs.airbyte.com/integrations/sources/postgres",
  "connectionSpecification": {
    "type": "object",
    "title": "Postgres Source Spec",
    "groups": [
      {
        "id": "db"
      },
      {
        "id": "auth"
      },
      {
        "id": "security",
        "title": "Security"
      },
      {
        "id": "advanced",
        "title": "Advanced"
      }
    ],
    "$schema": "http://json-schema.org/draft-07/schema#",
    "required": [
      "host",
      "port",
      "database",
      "username"
    ],
    "properties": {
      "host": {
        "type": "string",
        "group": "db",
        "order": 0,
        "title": "Host",
        "description": "Hostname of the database."
      },
      "port": {
        "type": "integer",
        "group": "db",
        "order": 1,
        "title": "Port",
        "default": 5432,
        "maximum": 65536,
        "minimum": 0,
        "examples": [
          "5432"
        ],
        "description": "Port of the database."
      },
      "schemas": {
        "type": "array",
        "group": "db",
        "items": {
          "type": "string"
        },
        "order": 3,
        "title": "Schemas",
        "default": [
          "public"
        ],
        "minItems": 0,
        "description": "The list of schemas (case sensitive) to sync from. Defaults to public.",
        "uniqueItems": true
      },
      "database": {
        "type": "string",
        "group": "db",
        "order": 2,
        "title": "Database Name",
        "description": "Name of the database."
      },
      "password": {
        "type": "string",
        "group": "auth",
        "order": 5,
        "title": "Password",
        "always_show": true,
        "description": "Password associated with the username.",
        "airbyte_secret": true
      },
      "ssl_mode": {
        "type": "object",
        "group": "security",
        "oneOf": [
          {
            "title": "disable",
            "required": [
              "mode"
            ],
            "properties": {
              "mode": {
                "type": "string",
                "const": "disable",
                "order": 0
              }
            },
            "description": "Disables encryption of communication between Airbyte and source database.",
            "additionalProperties": true
          },
          {
            "title": "allow",
            "required": [
              "mode"
            ],
            "properties": {
              "mode": {
                "type": "string",
                "const": "allow",
                "order": 0
              }
            },
            "description": "Enables encryption only when required by the source database.",
            "additionalProperties": true
          },
          {
            "title": "prefer",
            "required": [
              "mode"
            ],
            "properties": {
              "mode": {
                "type": "string",
                "const": "prefer",
                "order": 0
              }
            },
            "description": "Allows unencrypted connection only if the source database does not support encryption.",
            "additionalProperties": true
          },
          {
            "title": "require",
            "required": [
              "mode"
            ],
            "properties": {
              "mode": {
                "type": "string",
                "const": "require",
                "order": 0
              }
            },
            "description": "Always require encryption. If the source database server does not support encryption, connection will fail.",
            "additionalProperties": true
          },
          {
            "title": "verify-ca",
            "required": [
              "mode",
              "ca_certificate"
            ],
            "properties": {
              "mode": {
                "type": "string",
                "const": "verify-ca",
                "order": 0
              },
              "client_key": {
                "type": "string",
                "order": 3,
                "title": "Client Key",
                "multiline": true,
                "always_show": true,
                "description": "Client key",
                "airbyte_secret": true
              },
              "ca_certificate": {
                "type": "string",
                "order": 1,
                "title": "CA Certificate",
                "multiline": true,
                "description": "CA certificate",
                "airbyte_secret": true
              },
              "client_certificate": {
                "type": "string",
                "order": 2,
                "title": "Client Certificate",
                "multiline": true,
                "always_show": true,
                "description": "Client certificate",
                "airbyte_secret": true
              },
              "client_key_password": {
                "type": "string",
                "order": 4,
                "title": "Client key password",
                "description": "Password for keystorage. If you do not add it - the password will be generated automatically.",
                "airbyte_secret": true
              }
            },
            "description": "Always require encryption and verifies that the source database server has a valid SSL certificate.",
            "additionalProperties": true
          },
          {
            "title": "verify-full",
            "required": [
              "mode",
              "ca_certificate"
            ],
            "properties": {
              "mode": {
                "type": "string",
                "const": "verify-full",
                "order": 0
              },
              "client_key": {
                "type": "string",
                "order": 3,
                "title": "Client Key",
                "multiline": true,
                "always_show": true,
                "description": "Client key",
                "airbyte_secret": true
              },
              "ca_certificate": {
                "type": "string",
                "order": 1,
                "title": "CA Certificate",
                "multiline": true,
                "description": "CA certificate",
                "airbyte_secret": true
              },
              "client_certificate": {
                "type": "string",
                "order": 2,
                "title": "Client Certificate",
                "multiline": true,
                "always_show": true,
                "description": "Client certificate",
                "airbyte_secret": true
              },
              "client_key_password": {
                "type": "string",
                "order": 4,
                "title": "Client key password",
                "description": "Password for keystorage. If you do not add it - the password will be generated automatically.",
                "airbyte_secret": true
              }
            },
            "description": "This is the most secure mode. Always require encryption and verifies the identity of the source database server.",
            "additionalProperties": true
          }
        ],
        "order": 8,
        "title": "SSL Modes",
        "description": "SSL connection modes. \n  Read more <a href=\"https://jdbc.postgresql.org/documentation/head/ssl-client.html\"> in the docs</a>."
      },
      "username": {
        "type": "string",
        "group": "auth",
        "order": 4,
        "title": "Username",
        "description": "Username to access the database."
      },
      "tunnel_method": {
        "type": "object",
        "group": "security",
        "oneOf": [
          {
            "title": "No Tunnel",
            "required": [
              "tunnel_method"
            ],
            "properties": {
              "tunnel_method": {
                "type": "string",
                "const": "NO_TUNNEL",
                "order": 0,
                "description": "No ssh tunnel needed to connect to database"
              }
            }
          },
          {
            "title": "SSH Key Authentication",
            "required": [
              "tunnel_method",
              "tunnel_host",
              "tunnel_port",
              "tunnel_user",
              "ssh_key"
            ],
            "properties": {
              "ssh_key": {
                "type": "string",
                "order": 4,
                "title": "SSH Private Key",
                "multiline": true,
                "description": "OS-level user account ssh key credentials in RSA PEM format ( created with ssh-keygen -t rsa -m PEM -f myuser_rsa )",
                "airbyte_secret": true
              },
              "tunnel_host": {
                "type": "string",
                "order": 1,
                "title": "SSH Tunnel Jump Server Host",
                "description": "Hostname of the jump server host that allows inbound ssh tunnel."
              },
              "tunnel_port": {
                "type": "integer",
                "order": 2,
                "title": "SSH Connection Port",
                "default": 22,
                "maximum": 65536,
                "minimum": 0,
                "examples": [
                  "22"
                ],
                "description": "Port on the proxy/jump server that accepts inbound ssh connections."
              },
              "tunnel_user": {
                "type": "string",
                "order": 3,
                "title": "SSH Login Username",
                "description": "OS-level username for logging into the jump server host."
              },
              "tunnel_method": {
                "type": "string",
                "const": "SSH_KEY_AUTH",
                "order": 0,
                "description": "Connect through a jump server tunnel host using username and ssh key"
              }
            }
          },
          {
            "title": "Password Authentication",
            "required": [
              "tunnel_method",
              "tunnel_host",
              "tunnel_port",
              "tunnel_user",
              "tunnel_user_password"
            ],
            "properties": {
              "tunnel_host": {
                "type": "string",
                "order": 1,
                "title": "SSH Tunnel Jump Server Host",
                "description": "Hostname of the jump server host that allows inbound ssh tunnel."
              },
              "tunnel_port": {
                "type": "integer",
                "order": 2,
                "title": "SSH Connection Port",
                "default": 22,
                "maximum": 65536,
                "minimum": 0,
                "examples": [
                  "22"
                ],
                "description": "Port on the proxy/jump server that accepts inbound ssh connections."
              },
              "tunnel_user": {
                "type": "string",
                "order": 3,
                "title": "SSH Login Username",
                "description": "OS-level username for logging into the jump server host"
              },
              "tunnel_method": {
                "type": "string",
                "const": "SSH_PASSWORD_AUTH",
                "order": 0,
                "description": "Connect through a jump server tunnel host using username and password authentication"
              },
              "tunnel_user_password": {
                "type": "string",
                "order": 4,
                "title": "Password",
                "description": "OS-level password for logging into the jump server host",
                "airbyte_secret": true
              }
            }
          }
        ],
        "title": "SSH Tunnel Method",
        "description": "Whether to initiate an SSH tunnel before connecting to the database, and if so, which kind of authentication to use."
      },
      "jdbc_url_params": {
        "type": "string",
        "group": "advanced",
        "order": 6,
        "title": "JDBC URL Parameters (Advanced)",
        "description": "Additional properties to pass to the JDBC URL string when connecting to the database formatted as 'key=value' pairs separated by the symbol '&'. (Eg. key1=value1&key2=value2&key3=value3). For more information read about <a href=\"https://jdbc.postgresql.org/documentation/head/connect.html\">JDBC URL parameters</a>.",
        "pattern_descriptor": "key1=value1&key2=value2"
      },
      "replication_method": {
        "type": "object",
        "group": "advanced",
        "oneOf": [
          {
            "title": "Read Changes using Write-Ahead Log (CDC)",
            "required": [
              "method",
              "replication_slot",
              "publication"
            ],
            "properties": {
              "method": {
                "type": "string",
                "const": "CDC",
                "order": 1
              },
              "plugin": {
                "enum": [
                  "pgoutput"
                ],
                "type": "string",
                "order": 2,
                "title": "Plugin",
                "default": "pgoutput",
                "description": "A logical decoding plugin installed on the PostgreSQL server."
              },
              "queue_size": {
                "max": 10000,
                "min": 1000,
                "type": "integer",
                "order": 6,
                "title": "Size of the queue (Advanced)",
                "default": 10000,
                "description": "The size of the internal queue. This may interfere with memory consumption and efficiency of the connector, please be careful."
              },
              "publication": {
                "type": "string",
                "order": 4,
                "title": "Publication",
                "description": "A Postgres publication used for consuming changes. Read about <a href=\"https://docs.airbyte.com/integrations/sources/postgres#step-4-create-publications-and-replication-identities-for-tables\">publications and replication identities</a>."
              },
              "replication_slot": {
                "type": "string",
                "order": 3,
                "title": "Replication Slot",
                "description": "A plugin logical replication slot. Read about <a href=\"https://docs.airbyte.com/integrations/sources/postgres#step-3-create-replication-slot\">replication slots</a>."
              },
              "lsn_commit_behaviour": {
                "enum": [
                  "While reading Data",
                  "After loading Data in the destination"
                ],
                "type": "string",
                "order": 7,
                "title": "LSN commit behaviour",
                "default": "After loading Data in the destination",
                "description": "Determines when Airbtye should flush the LSN of processed WAL logs in the source database. `After loading Data in the destination` is default. If `While reading Data` is selected, in case of a downstream failure (while loading data into the destination), next sync would result in a full sync."
              },
              "initial_waiting_seconds": {
                "max": 1200,
                "min": 120,
                "type": "integer",
                "order": 5,
                "title": "Initial Waiting Time in Seconds (Advanced)",
                "default": 300,
                "description": "The amount of time the connector will wait when it launches to determine if there is new data to sync or not. Defaults to 300 seconds. Valid range: 120 seconds to 1200 seconds. Read about <a href=\"https://docs.airbyte.com/integrations/sources/postgres#step-5-optional-set-up-initial-waiting-time\">initial waiting time</a>."
              }
            },
            "description": "<i>Recommended</i> - Incrementally reads new inserts, updates, and deletes using the Postgres <a href=\"https://docs.airbyte.com/integrations/sources/postgres/#cdc\">write-ahead log (WAL)</a>. This needs to be configured on the source database itself. Recommended for tables of any size.",
            "additionalProperties": true
          },
          {
            "title": "Detect Changes with Xmin System Column",
            "required": [
              "method"
            ],
            "properties": {
              "method": {
                "type": "string",
                "const": "Xmin",
                "order": 0
              }
            },
            "description": "<i>Recommended</i> - Incrementally reads new inserts and updates via Postgres <a href=\"https://docs.airbyte.com/integrations/sources/postgres/#xmin\">Xmin system column</a>. Only recommended for tables up to 500GB."
          },
          {
            "title": "Scan Changes with User Defined Cursor",
            "required": [
              "method"
            ],
            "properties": {
              "method": {
                "type": "string",
                "const": "Standard",
                "order": 8
              }
            },
            "description": "Incrementally detects new inserts and updates using the <a href=\"https://docs.airbyte.com/understanding-airbyte/connections/incremental-append/#user-defined-cursor\">cursor column</a> chosen when configuring a connection (e.g. created_at, updated_at)."
          }
        ],
        "order": 9,
        "title": "Update Method",
        "default": "CDC",
        "description": "Configures how data is extracted from the database.",
        "display_type": "radio"
      }
    }
  },
  "jobInfo": {
    "id": "2da5e27b-427d-4484-8e4e-72f2eb6c6bf7",
    "configType": "get_spec",
    "configId": "Optional.empty",
    "createdAt": 1705523876895,
    "endedAt": 1705523876895,
    "succeeded": true,
    "connectorConfigurationUpdated": false,
    "logs": {
      "logLines": []
    }
  }
}