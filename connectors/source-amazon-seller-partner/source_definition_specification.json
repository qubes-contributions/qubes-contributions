{
  "sourceDefinitionId": "e55879a8-0ef8-4557-abcf-ab34c53ec460",
  "documentationUrl": "https://docs.airbyte.com/integrations/sources/amazon-seller-partner",
  "connectionSpecification": {
    "type": "object",
    "title": "Amazon Seller Partner Spec",
    "required": [
      "aws_environment",
      "region",
      "lwa_app_id",
      "lwa_client_secret",
      "refresh_token",
      "replication_start_date"
    ],
    "properties": {
      "region": {
        "enum": [
          "AE",
          "AU",
          "BE",
          "BR",
          "CA",
          "DE",
          "EG",
          "ES",
          "FR",
          "GB",
          "IN",
          "IT",
          "JP",
          "MX",
          "NL",
          "PL",
          "SA",
          "SE",
          "SG",
          "TR",
          "UK",
          "US"
        ],
        "type": "string",
        "order": 2,
        "title": "AWS Region",
        "default": "US",
        "description": "Select the AWS Region."
      },
      "role_arn": {
        "type": "string",
        "order": 5,
        "title": "Role ARN",
        "description": "Specifies the Amazon Resource Name (ARN) of an IAM role that you want to use to perform operations requested using this profile. (Needs permission to 'Assume Role' STS).",
        "airbyte_secret": true
      },
      "auth_type": {
        "type": "string",
        "const": "oauth2.0",
        "order": 0,
        "title": "Auth Type"
      },
      "lwa_app_id": {
        "type": "string",
        "order": 6,
        "title": "LWA Client Id",
        "description": "Your Login with Amazon Client ID.",
        "airbyte_secret": true
      },
      "refresh_token": {
        "type": "string",
        "order": 8,
        "title": "Refresh Token",
        "description": "The Refresh Token obtained via OAuth flow authorization.",
        "airbyte_secret": true
      },
      "aws_access_key": {
        "type": "string",
        "order": 3,
        "title": "AWS Access Key",
        "description": "Specifies the AWS access key used as part of the credentials to authenticate the user.",
        "airbyte_secret": true
      },
      "aws_secret_key": {
        "type": "string",
        "order": 4,
        "title": "AWS Secret Access Key",
        "description": "Specifies the AWS secret key used as part of the credentials to authenticate the user.",
        "airbyte_secret": true
      },
      "period_in_days": {
        "type": "integer",
        "order": 11,
        "title": "Period In Days",
        "default": 90,
        "description": "Will be used for stream slicing for initial full_refresh sync when no updated state is present for reports that support sliced incremental sync."
      },
      "report_options": {
        "type": "string",
        "order": 12,
        "title": "Report Options",
        "examples": [
          "{\"GET_BRAND_ANALYTICS_SEARCH_TERMS_REPORT\": {\"reportPeriod\": \"WEEK\"}}",
          "{\"GET_SOME_REPORT\": {\"custom\": \"true\"}}"
        ],
        "description": "Additional information passed to reports. This varies by report type. Must be a valid json string."
      },
      "aws_environment": {
        "enum": [
          "PRODUCTION",
          "SANDBOX"
        ],
        "type": "string",
        "order": 1,
        "title": "AWS Environment",
        "default": "PRODUCTION",
        "description": "Select the AWS Environment."
      },
      "max_wait_seconds": {
        "type": "integer",
        "order": 13,
        "title": "Max wait time for reports (in seconds)",
        "default": 500,
        "examples": [
          "500",
          "1980"
        ],
        "description": "Sometimes report can take up to 30 minutes to generate. This will set the limit for how long to wait for a successful report."
      },
      "lwa_client_secret": {
        "type": "string",
        "order": 7,
        "title": "LWA Client Secret",
        "description": "Your Login with Amazon Client Secret.",
        "airbyte_secret": true
      },
      "replication_end_date": {
        "type": "string",
        "order": 10,
        "title": "End Date",
        "pattern": "^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}Z$|^$",
        "examples": [
          "2017-01-25T00:00:00Z"
        ],
        "description": "UTC date and time in the format 2017-01-25T00:00:00Z. Any data after this date will not be replicated."
      },
      "replication_start_date": {
        "type": "string",
        "order": 9,
        "title": "Start Date",
        "pattern": "^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}Z$",
        "examples": [
          "2017-01-25T00:00:00Z"
        ],
        "description": "UTC date and time in the format 2017-01-25T00:00:00Z. Any data before this date will not be replicated."
      },
      "advanced_stream_options": {
        "type": "string",
        "order": 14,
        "title": "Advanced Stream Options",
        "examples": [
          "{\"GET_SALES_AND_TRAFFIC_REPORT\": {\"availability_sla_days\": 3}}",
          "{\"GET_SOME_REPORT\": {\"custom\": \"true\"}}"
        ],
        "description": "Additional information to configure report options. This varies by report type, not every report implement this kind of feature. Must be a valid json string."
      }
    },
    "additionalProperties": true
  },
  "advancedAuth": {
    "authFlowType": "oauth2.0",
    "predicateKey": [
      "auth_type"
    ],
    "predicateValue": "oauth2.0",
    "oauthConfigSpecification": {
      "completeOAuthOutputSpecification": {
        "type": "object",
        "properties": {
          "refresh_token": {
            "type": "string",
            "path_in_connector_config": [
              "refresh_token"
            ]
          }
        },
        "additionalProperties": false
      },
      "completeOAuthServerInputSpecification": {
        "type": "object",
        "properties": {
          "lwa_app_id": {
            "type": "string"
          },
          "lwa_client_secret": {
            "type": "string"
          }
        },
        "additionalProperties": false
      },
      "completeOAuthServerOutputSpecification": {
        "type": "object",
        "properties": {
          "lwa_app_id": {
            "type": "string",
            "path_in_connector_config": [
              "lwa_app_id"
            ]
          },
          "lwa_client_secret": {
            "type": "string",
            "path_in_connector_config": [
              "lwa_client_secret"
            ]
          }
        },
        "additionalProperties": false
      }
    }
  },
  "jobInfo": {
    "id": "a2de4176-1e40-41d3-a4a2-b008e3b47a1d",
    "configType": "get_spec",
    "configId": "Optional.empty",
    "createdAt": 1705523883405,
    "endedAt": 1705523883405,
    "succeeded": true,
    "connectorConfigurationUpdated": false,
    "logs": {
      "logLines": []
    }
  }
}