{
  "sourceDefinitionId": "2a8c41ae-8c23-4be0-a73f-2ab10ca1a820",
  "documentationUrl": "https://docs.airbyte.com/integrations/sources/gcs",
  "connectionSpecification": {
    "type": "object",
    "title": "Config",
    "required": [
      "streams",
      "service_account",
      "bucket"
    ],
    "properties": {
      "bucket": {
        "type": "string",
        "order": 2,
        "title": "Bucket",
        "description": "Name of the GCS bucket where the file(s) exist."
      },
      "streams": {
        "type": "array",
        "items": {
          "type": "object",
          "title": "SourceGCSStreamConfig",
          "required": [
            "name",
            "format"
          ],
          "properties": {
            "name": {
              "type": "string",
              "order": 0,
              "title": "Name",
              "description": "The name of the stream."
            },
            "globs": {
              "type": "array",
              "items": {
                "type": "string"
              },
              "order": 1,
              "title": "Globs",
              "description": "The pattern used to specify which files should be selected from the file system. For more information on glob pattern matching look <a href=\"https://en.wikipedia.org/wiki/Glob_(programming)\">here</a>."
            },
            "format": {
              "type": "object",
              "oneOf": [
                {
                  "type": "object",
                  "title": "CSV Format",
                  "properties": {
                    "encoding": {
                      "type": "string",
                      "title": "Encoding",
                      "default": "utf8",
                      "description": "The character encoding of the CSV data. Leave blank to default to <strong>UTF8</strong>. See <a href=\"https://docs.python.org/3/library/codecs.html#standard-encodings\" target=\"_blank\">list of python encodings</a> for allowable options."
                    },
                    "filetype": {
                      "type": "string",
                      "const": "csv",
                      "title": "Filetype",
                      "default": "csv"
                    },
                    "delimiter": {
                      "type": "string",
                      "title": "Delimiter",
                      "default": ",",
                      "description": "The character delimiting individual cells in the CSV data. This may only be a 1-character string. For tab-delimited data enter '\\t'."
                    },
                    "quote_char": {
                      "type": "string",
                      "title": "Quote Character",
                      "default": "\"",
                      "description": "The character used for quoting CSV values. To disallow quoting, make this field blank."
                    },
                    "escape_char": {
                      "type": "string",
                      "title": "Escape Character",
                      "description": "The character used for escaping special characters. To disallow escaping, leave this field blank."
                    },
                    "null_values": {
                      "type": "array",
                      "items": {
                        "type": "string"
                      },
                      "title": "Null Values",
                      "default": [],
                      "description": "A set of case-sensitive strings that should be interpreted as null values. For example, if the value 'NA' should be interpreted as null, enter 'NA' in this field.",
                      "uniqueItems": true
                    },
                    "true_values": {
                      "type": "array",
                      "items": {
                        "type": "string"
                      },
                      "title": "True Values",
                      "default": [
                        "y",
                        "yes",
                        "t",
                        "true",
                        "on",
                        "1"
                      ],
                      "description": "A set of case-sensitive strings that should be interpreted as true values.",
                      "uniqueItems": true
                    },
                    "double_quote": {
                      "type": "boolean",
                      "title": "Double Quote",
                      "default": true,
                      "description": "Whether two quotes in a quoted CSV value denote a single quote in the data."
                    },
                    "false_values": {
                      "type": "array",
                      "items": {
                        "type": "string"
                      },
                      "title": "False Values",
                      "default": [
                        "n",
                        "no",
                        "f",
                        "false",
                        "off",
                        "0"
                      ],
                      "description": "A set of case-sensitive strings that should be interpreted as false values.",
                      "uniqueItems": true
                    },
                    "inference_type": {
                      "enum": [
                        "None",
                        "Primitive Types Only"
                      ],
                      "title": "Inference Type",
                      "default": "None",
                      "description": "How to infer the types of the columns. If none, inference default to strings.",
                      "airbyte_hidden": true
                    },
                    "header_definition": {
                      "type": "object",
                      "oneOf": [
                        {
                          "type": "object",
                          "title": "From CSV",
                          "properties": {
                            "header_definition_type": {
                              "type": "string",
                              "const": "From CSV",
                              "title": "Header Definition Type",
                              "default": "From CSV"
                            }
                          }
                        },
                        {
                          "type": "object",
                          "title": "Autogenerated",
                          "properties": {
                            "header_definition_type": {
                              "type": "string",
                              "const": "Autogenerated",
                              "title": "Header Definition Type",
                              "default": "Autogenerated"
                            }
                          }
                        },
                        {
                          "type": "object",
                          "title": "User Provided",
                          "required": [
                            "column_names"
                          ],
                          "properties": {
                            "column_names": {
                              "type": "array",
                              "items": {
                                "type": "string"
                              },
                              "title": "Column Names",
                              "description": "The column names that will be used while emitting the CSV records"
                            },
                            "header_definition_type": {
                              "type": "string",
                              "const": "User Provided",
                              "title": "Header Definition Type",
                              "default": "User Provided"
                            }
                          }
                        }
                      ],
                      "title": "CSV Header Definition",
                      "default": {
                        "header_definition_type": "From CSV"
                      },
                      "description": "How headers will be defined. `User Provided` assumes the CSV does not have a header row and uses the headers provided and `Autogenerated` assumes the CSV does not have a header row and the CDK will generate headers using for `f{i}` where `i` is the index starting from 0. Else, the default behavior is to use the header from the CSV file. If a user wants to autogenerate or provide column names for a CSV having headers, they can skip rows."
                    },
                    "strings_can_be_null": {
                      "type": "boolean",
                      "title": "Strings Can Be Null",
                      "default": true,
                      "description": "Whether strings can be interpreted as null values. If true, strings that match the null_values set will be interpreted as null. If false, strings that match the null_values set will be interpreted as the string itself."
                    },
                    "skip_rows_after_header": {
                      "type": "integer",
                      "title": "Skip Rows After Header",
                      "default": 0,
                      "description": "The number of rows to skip after the header row."
                    },
                    "skip_rows_before_header": {
                      "type": "integer",
                      "title": "Skip Rows Before Header",
                      "default": 0,
                      "description": "The number of rows to skip before the header row. For example, if the header row is on the 3rd row, enter 2 in this field."
                    }
                  }
                }
              ],
              "order": 2,
              "title": "Format",
              "description": "The configuration options that are used to alter how to read incoming files that deviate from the standard formatting."
            },
            "schemaless": {
              "type": "boolean",
              "title": "Schemaless",
              "default": false,
              "description": "When enabled, syncs will not validate or structure records against the stream's schema."
            },
            "primary_key": {
              "type": "string",
              "title": "Primary Key",
              "description": "The column or columns (for a composite key) that serves as the unique identifier of a record."
            },
            "input_schema": {
              "type": "string",
              "title": "Input Schema",
              "description": "The schema that will be used to validate records extracted from the file. This will override the stream schema that is auto-detected from incoming files."
            },
            "legacy_prefix": {
              "type": "string",
              "title": "Legacy Prefix",
              "description": "The path prefix configured in previous versions of the GCS connector. This option is deprecated in favor of a single glob.",
              "airbyte_hidden": true
            },
            "validation_policy": {
              "enum": [
                "Emit Record",
                "Skip Record",
                "Wait for Discover"
              ],
              "title": "Validation Policy",
              "default": "Emit Record",
              "description": "The name of the validation policy that dictates sync behavior when a record does not adhere to the stream schema."
            },
            "days_to_sync_if_history_is_full": {
              "type": "integer",
              "title": "Days To Sync If History Is Full",
              "default": 3,
              "description": "When the state history of the file store is full, syncs will only read files that were last modified in the provided day range."
            }
          }
        },
        "order": 3,
        "title": "The list of streams to sync",
        "description": "Each instance of this configuration defines a <a href=https://docs.airbyte.com/cloud/core-concepts#stream>stream</a>. Use this to define which files belong in the stream, their format, and how they should be parsed and validated. When sending data to warehouse destination such as Snowflake or BigQuery, each stream is a separate table."
      },
      "start_date": {
        "type": "string",
        "order": 1,
        "title": "Start Date",
        "format": "date-time",
        "pattern": "^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{6}Z$",
        "examples": [
          "2021-01-01T00:00:00.000000Z"
        ],
        "description": "UTC date and time in the format 2017-01-25T00:00:00.000000Z. Any file modified before this date will not be replicated.",
        "pattern_descriptor": "YYYY-MM-DDTHH:mm:ss.SSSSSSZ"
      },
      "service_account": {
        "type": "string",
        "order": 0,
        "title": "Service Account Information",
        "description": "Enter your Google Cloud <a href=\"https://cloud.google.com/iam/docs/creating-managing-service-account-keys#creating_service_account_keys\">service account key</a> in JSON format",
        "airbyte_secret": true
      }
    },
    "description": "NOTE: When this Spec is changed, legacy_config_transformer.py must also be\nmodified to uptake the changes because it is responsible for converting\nlegacy GCS configs into file based configs using the File-Based CDK."
  },
  "jobInfo": {
    "id": "1399b48b-1b94-43e3-9d0c-5bc479b5f007",
    "configType": "get_spec",
    "configId": "Optional.empty",
    "createdAt": 1705523887669,
    "endedAt": 1705523887669,
    "succeeded": true,
    "connectorConfigurationUpdated": false,
    "logs": {
      "logLines": []
    }
  }
}