qubeKey: source-google-analytics-v4
name: source-google-analytics-v4
type: airbyte-source
id: source-google-analytics-v4
maturity: generally_available
ui:
  icon: <svg xmlns="http://www.w3.org/2000/svg" width="250" height="250" fill="none"><path
    fill="#F9AB00" d="M163.287 50.691v147.055c0 16.454 11.334 25.623 23.357 25.623
    11.123 0 23.357-7.798 23.357-25.623V51.806c0-15.085-11.122-24.51-23.357-24.51-12.234
    0-23.357 10.394-23.357 23.395Z"/><path fill="#E37400" d="M102.111 125.333v72.413c0
    16.454 11.334 25.623 23.357 25.623 11.123 0 23.357-7.798 23.357-25.623v-71.299c0-15.085-11.122-24.509-23.357-24.509-12.234
    0-23.357 10.394-23.357 23.395Zm-37.815 98.036c12.9 0 23.357-10.474 23.357-23.395s-10.457-23.395-23.357-23.395c-12.9
    0-23.357 10.474-23.357 23.395s10.458 23.395 23.357 23.395Z"/></svg>
  shortDescription: api
  description: ''
  links:
  - linkName: Documentation
    url: https://docs.airbyte.com/integrations/sources/google-analytics-v4
properties:
  type: object
  title: Google Analytics (V4) Spec
  schema: http://json-schema.org/draft-07/schema#
  required:
  - view_id
  - start_date
  properties:
    view_id:
      type: string
      order: 2
      title: View ID
      description: The ID for the Google Analytics View you want to fetch data from.
        This can be found from the <a href="https://ga-dev-tools.appspot.com/account-explorer/">Google
        Analytics Account Explorer</a>.
    start_date:
      type: string
      order: 1
      title: Replication Start Date
      format: date
      pattern: ^[0-9]{4}-[0-9]{2}-[0-9]{2}$|^$|[\s\S]+$
      examples:
      - '2020-06-01'
      description: The date in the format YYYY-MM-DD. Any data before this date will
        not be replicated.
    credentials:
      type: object
      oneOf:
      - type: object
        title: Authenticate via Google (Oauth)
        required:
        - client_id
        - client_secret
        - refresh_token
        properties:
          auth_type:
            type: string
            const: Client
            order: 0
          client_id:
            type: string
            order: 1
            title: Client ID
            description: The Client ID of your Google Analytics developer application.
            airbyte_secret: true
          access_token:
            type: string
            order: 4
            title: Access Token
            description: Access Token for making authenticated requests.
            airbyte_secret: true
          client_secret:
            type: string
            order: 2
            title: Client Secret
            description: The Client Secret of your Google Analytics developer application.
            airbyte_secret: true
          refresh_token:
            type: string
            order: 3
            title: Refresh Token
            description: The token for obtaining a new access token.
            airbyte_secret: true
      - type: object
        title: Service Account Key Authentication
        required:
        - credentials_json
        properties:
          auth_type:
            type: string
            const: Service
            order: 0
          credentials_json:
            type: string
            title: Service Account JSON Key
            examples:
            - '{ "type": "service_account", "project_id": YOUR_PROJECT_ID, "private_key_id":
              YOUR_PRIVATE_KEY, ... }'
            description: The JSON key of the service account to use for authorization
            airbyte_secret: true
      order: 0
      title: Credentials
      description: Credentials for the service
    custom_reports:
      type: string
      order: 3
      title: Custom Reports
      description: A JSON array describing the custom reports you want to sync from
        Google Analytics. See <a href="https://docs.airbyte.com/integrations/sources/google-analytics-v4#data-processing-latency">the
        docs</a> for more information about the exact format you can use to fill out
        this field.
    window_in_days:
      type: integer
      order: 4
      title: Data request time increment in days
      default: 1
      examples:
      - 30
      - 60
      - 90
      - 120
      - 200
      - 364
      description: 'The time increment used by the connector when requesting data
        from the Google Analytics API. More information is available in the <a href="https://docs.airbyte.com/integrations/sources/google-analytics-v4/#sampling-in-reports">the
        docs</a>. The bigger this value is, the faster the sync will be, but the more
        likely that sampling will be applied to your data, potentially causing inaccuracies
        in the returned results. We recommend setting this to 1 unless you have a
        hard requirement to make the sync faster at the expense of accuracy. The minimum
        allowed value for this field is 1, and the maximum is 364. '
  additionalProperties: true
schemaType: {}
