{
  "sourceDefinitionId": "12928b32-bf0a-4f1e-964f-07e12e37153a",
  "documentationUrl": "https://docs.airbyte.com/integrations/sources/mixpanel",
  "connectionSpecification": {
    "type": "object",
    "title": "Source Mixpanel Spec",
    "$schema": "http://json-schema.org/draft-07/schema#",
    "required": [
      "credentials"
    ],
    "properties": {
      "region": {
        "enum": [
          "US",
          "EU"
        ],
        "type": "string",
        "order": 7,
        "title": "Region",
        "default": "US",
        "description": "The region of mixpanel domain instance either US or EU."
      },
      "end_date": {
        "type": "string",
        "order": 6,
        "title": "End Date",
        "format": "date",
        "pattern": "^$|^[0-9]{4}-[0-9]{2}-[0-9]{2}(T[0-9]{2}:[0-9]{2}:[0-9]{2}Z)?$",
        "examples": [
          "2021-11-16"
        ],
        "description": "The date in the format YYYY-MM-DD. Any data after this date will not be replicated. Left empty to always sync to most recent date"
      },
      "start_date": {
        "type": "string",
        "order": 5,
        "title": "Start Date",
        "format": "date",
        "pattern": "^$|^[0-9]{4}-[0-9]{2}-[0-9]{2}(T[0-9]{2}:[0-9]{2}:[0-9]{2}Z)?$",
        "examples": [
          "2021-11-16"
        ],
        "description": "The date in the format YYYY-MM-DD. Any data before this date will not be replicated. If this option is not set, the connector will replicate data from up to one year ago by default."
      },
      "credentials": {
        "type": "object",
        "oneOf": [
          {
            "type": "object",
            "title": "Service Account",
            "required": [
              "username",
              "secret",
              "project_id"
            ],
            "properties": {
              "secret": {
                "type": "string",
                "order": 2,
                "title": "Secret",
                "description": "Mixpanel Service Account Secret. See the <a href=\"https://developer.mixpanel.com/reference/service-accounts\">docs</a> for more information on how to obtain this.",
                "airbyte_secret": true
              },
              "username": {
                "type": "string",
                "order": 1,
                "title": "Username",
                "description": "Mixpanel Service Account Username. See the <a href=\"https://developer.mixpanel.com/reference/service-accounts\">docs</a> for more information on how to obtain this."
              },
              "project_id": {
                "type": "integer",
                "order": 3,
                "title": "Project ID",
                "description": "Your project ID number. See the <a href=\"https://help.mixpanel.com/hc/en-us/articles/115004490503-Project-Settings#project-id\">docs</a> for more information on how to obtain this."
              },
              "option_title": {
                "type": "string",
                "const": "Service Account",
                "order": 0
              }
            }
          },
          {
            "type": "object",
            "title": "Project Secret",
            "required": [
              "api_secret"
            ],
            "properties": {
              "api_secret": {
                "type": "string",
                "order": 1,
                "title": "Project Secret",
                "description": "Mixpanel project secret. See the <a href=\"https://developer.mixpanel.com/reference/project-secret#managing-a-projects-secret\">docs</a> for more information on how to obtain this.",
                "airbyte_secret": true
              },
              "option_title": {
                "type": "string",
                "const": "Project Secret",
                "order": 0
              }
            }
          }
        ],
        "order": 0,
        "title": "Authentication *",
        "description": "Choose how to authenticate to Mixpanel"
      },
      "date_window_size": {
        "type": "integer",
        "order": 8,
        "title": "Date slicing window",
        "default": 30,
        "minimum": 1,
        "description": "Defines window size in days, that used to slice through data. You can reduce it, if amount of data in each window is too big for your environment. (This value should be positive integer)"
      },
      "project_timezone": {
        "type": "string",
        "order": 3,
        "title": "Project Timezone",
        "default": "US/Pacific",
        "examples": [
          "US/Pacific",
          "UTC"
        ],
        "description": "Time zone in which integer date times are stored. The project timezone may be found in the project settings in the <a href=\"https://help.mixpanel.com/hc/en-us/articles/115004547203-Manage-Timezones-for-Projects-in-Mixpanel\">Mixpanel console</a>."
      },
      "attribution_window": {
        "type": "integer",
        "order": 2,
        "title": "Attribution Window",
        "default": 5,
        "description": "A period of time for attributing results to ads and the lookback period after those actions occur during which ad results are counted. Default attribution window is 5 days. (This value should be non-negative integer)"
      },
      "select_properties_by_default": {
        "type": "boolean",
        "order": 4,
        "title": "Select Properties By Default",
        "default": true,
        "description": "Setting this config parameter to TRUE ensures that new properties on events and engage records are captured. Otherwise new properties will be ignored."
      }
    }
  },
  "jobInfo": {
    "id": "37199a69-7527-488d-8be7-496776d77c4d",
    "configType": "get_spec",
    "configId": "Optional.empty",
    "createdAt": 1705523885355,
    "endedAt": 1705523885355,
    "succeeded": true,
    "connectorConfigurationUpdated": false,
    "logs": {
      "logLines": []
    }
  }
}