{
  "sourceDefinitionId": "c6b0a29e-1da9-4512-9002-7bfd0cba2246",
  "documentationUrl": "https://docs.airbyte.com/integrations/sources/amazon-ads",
  "connectionSpecification": {
    "type": "object",
    "title": "Amazon Ads Spec",
    "required": [
      "client_id",
      "client_secret",
      "refresh_token"
    ],
    "properties": {
      "region": {
        "enum": [
          "NA",
          "EU",
          "FE"
        ],
        "type": "string",
        "order": 4,
        "title": "Region",
        "default": "NA",
        "description": "Region to pull data from (EU/NA/FE). See <a href=\"https://advertising.amazon.com/API/docs/en-us/info/api-overview#api-endpoints\">docs</a> for more details."
      },
      "profiles": {
        "type": "array",
        "items": {
          "type": "integer"
        },
        "order": 6,
        "title": "Profile IDs",
        "description": "Profile IDs you want to fetch data for. See <a href=\"https://advertising.amazon.com/API/docs/en-us/concepts/authorization/profiles\">docs</a> for more details. Note: If Marketplace IDs are also selected, profiles will be selected if they match the Profile ID OR the Marketplace ID."
      },
      "auth_type": {
        "type": "string",
        "const": "oauth2.0",
        "order": 0,
        "title": "Auth Type"
      },
      "client_id": {
        "type": "string",
        "order": 1,
        "title": "Client ID",
        "description": "The client ID of your Amazon Ads developer application. See the <a href=\"https://advertising.amazon.com/API/docs/en-us/get-started/generate-api-tokens#retrieve-your-client-id-and-client-secret\">docs</a> for more information.",
        "airbyte_secret": true
      },
      "start_date": {
        "type": "string",
        "order": 5,
        "title": "Start Date",
        "format": "date",
        "pattern": "^[0-9]{4}-[0-9]{2}-[0-9]{2}$",
        "examples": [
          "2022-10-10",
          "2022-10-22"
        ],
        "description": "The Start date for collecting reports, should not be more than 60 days in the past. In YYYY-MM-DD format"
      },
      "state_filter": {
        "type": "array",
        "items": {
          "enum": [
            "enabled",
            "paused",
            "archived"
          ],
          "type": "string"
        },
        "order": 8,
        "title": "State Filter",
        "description": "Reflects the state of the Display, Product, and Brand Campaign streams as enabled, paused, or archived. If you do not populate this field, it will be ignored completely.",
        "uniqueItems": true
      },
      "client_secret": {
        "type": "string",
        "order": 2,
        "title": "Client Secret",
        "description": "The client secret of your Amazon Ads developer application. See the <a href=\"https://advertising.amazon.com/API/docs/en-us/get-started/generate-api-tokens#retrieve-your-client-id-and-client-secret\">docs</a> for more information.",
        "airbyte_secret": true
      },
      "refresh_token": {
        "type": "string",
        "order": 3,
        "title": "Refresh Token",
        "description": "Amazon Ads refresh token. See the <a href=\"https://advertising.amazon.com/API/docs/en-us/get-started/generate-api-tokens\">docs</a> for more information on how to obtain this token.",
        "airbyte_secret": true
      },
      "marketplace_ids": {
        "type": "array",
        "items": {
          "type": "string"
        },
        "order": 7,
        "title": "Marketplace IDs",
        "description": "Marketplace IDs you want to fetch data for. Note: If Profile IDs are also selected, profiles will be selected if they match the Profile ID OR the Marketplace ID."
      },
      "look_back_window": {
        "type": "integer",
        "order": 9,
        "title": "Look Back Window",
        "default": 3,
        "examples": [
          3,
          10
        ],
        "description": "The amount of days to go back in time to get the updated data from Amazon Ads"
      },
      "report_record_types": {
        "type": "array",
        "items": {
          "enum": [
            "adGroups",
            "asins",
            "asins_keywords",
            "asins_targets",
            "campaigns",
            "keywords",
            "productAds",
            "targets"
          ],
          "type": "string"
        },
        "order": 10,
        "title": "Report Record Types",
        "description": "Optional configuration which accepts an array of string of record types. Leave blank for default behaviour to pull all report types. Use this config option only if you want to pull specific report type(s). See <a href=\"https://advertising.amazon.com/API/docs/en-us/reporting/v2/report-types\">docs</a> for more details",
        "uniqueItems": true
      }
    },
    "additionalProperties": true
  },
  "advancedAuth": {
    "authFlowType": "oauth2.0",
    "predicateKey": [
      "auth_type"
    ],
    "predicateValue": "oauth2.0",
    "oauthConfigSpecification": {
      "completeOAuthOutputSpecification": {
        "type": "object",
        "properties": {
          "refresh_token": {
            "type": "string",
            "path_in_connector_config": [
              "refresh_token"
            ]
          }
        },
        "additionalProperties": true
      },
      "completeOAuthServerInputSpecification": {
        "type": "object",
        "properties": {
          "client_id": {
            "type": "string"
          },
          "client_secret": {
            "type": "string"
          }
        },
        "additionalProperties": true
      },
      "completeOAuthServerOutputSpecification": {
        "type": "object",
        "properties": {
          "client_id": {
            "type": "string",
            "path_in_connector_config": [
              "client_id"
            ]
          },
          "client_secret": {
            "type": "string",
            "path_in_connector_config": [
              "client_secret"
            ]
          }
        },
        "additionalProperties": true
      }
    }
  },
  "jobInfo": {
    "id": "d1031f8c-4644-4016-9350-6680b6832de6",
    "configType": "get_spec",
    "configId": "Optional.empty",
    "createdAt": 1705523879772,
    "endedAt": 1705523879772,
    "succeeded": true,
    "connectorConfigurationUpdated": false,
    "logs": {
      "logLines": []
    }
  }
}