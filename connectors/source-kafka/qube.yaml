qubeKey: source-kafka
name: source-kafka
type: airbyte-source
id: source-kafka
maturity: alpha
ui:
  icon: <svg xmlns="http://www.w3.org/2000/svg" width="250" height="250" fill="none"><g
    clip-path="url(#a)"><path fill="#000" d="M167.675 137.813c-9.358 0-17.747 4.136-23.492
    10.647l-14.72-10.396c1.562-4.292 2.459-8.901 2.459-13.725 0-4.74-.866-9.272-2.377-13.501l14.688-10.286c5.744
    6.479 14.112 10.592 23.442 10.592 17.271 0 31.325-14.018 31.325-31.25 0-17.233-14.054-31.251-31.325-31.251-17.272
    0-31.325 14.018-31.325 31.25 0 3.085.467 6.06 1.305 8.876l-14.698 10.292a40.451
    40.451 0 0 0-25.051-14.524V66.865c14.189-2.973 24.88-15.55 24.88-30.578 0-17.233-14.053-31.251-31.325-31.251-17.271
    0-31.325 14.018-31.325 31.25 0 14.828 10.414 27.25 24.32 30.435v17.901C65.48 87.946
    51 104.468 51 124.34c0 19.968 14.622 36.551 33.737 39.761v18.903c-14.048 3.08-24.6
    15.578-24.6 30.506 0 17.233 14.053 31.251 31.324 31.251 17.272 0 31.325-14.018
    31.325-31.251 0-14.928-10.552-27.426-24.6-30.506V164.1a40.47 40.47 0 0 0 24.628-14.29l14.819
    10.464a31.056 31.056 0 0 0-1.283 8.79c0 17.232 14.053 31.25 31.325 31.25 17.271
    0 31.325-14.018 31.325-31.25 0-17.233-14.054-31.251-31.325-31.251Zm0-73.071c8.375
    0 15.187 6.798 15.187 15.152 0 8.353-6.812 15.15-15.187 15.15s-15.187-6.797-15.187-15.15c0-8.354
    6.812-15.152 15.187-15.152ZM76.273 36.287c0-8.353 6.813-15.152 15.188-15.152 8.376
    0 15.188 6.799 15.188 15.152 0 8.353-6.813 15.15-15.188 15.15s-15.188-6.797-15.188-15.15Zm30.376
    177.222c0 8.353-6.813 15.151-15.188 15.151s-15.188-6.798-15.188-15.151 6.813-15.151
    15.188-15.151c8.376 0 15.188 6.798 15.188 15.151ZM91.46 145.47c-11.68 0-21.184-9.478-21.184-21.131s9.503-21.133
    21.184-21.133c11.681 0 21.183 9.48 21.183 21.133 0 11.653-9.502 21.131-21.183
    21.131Zm76.215 38.746c-8.375 0-15.187-6.799-15.187-15.152 0-8.353 6.812-15.151
    15.187-15.151s15.187 6.798 15.187 15.151-6.812 15.152-15.187 15.152Z"/></g><defs><clipPath
    id="a"><path fill="#fff" d="M51 5h148v240H51z"/></clipPath></defs></svg>
  shortDescription: database
  description: ''
  links:
  - linkName: Documentation
    url: https://docs.airbyte.com/integrations/sources/kafka
properties:
  type: object
  title: Kafka Source Spec
  schema: http://json-schema.org/draft-07/schema#
  required:
  - bootstrap_servers
  - subscription
  - protocol
  properties:
    group_id:
      type: string
      title: Group ID
      examples:
      - group.id
      description: The Group ID is how you distinguish different consumer groups.
    protocol:
      type: object
      oneOf:
      - title: PLAINTEXT
        required:
        - security_protocol
        properties:
          security_protocol:
            type: string
            const: PLAINTEXT
      - title: SASL PLAINTEXT
        required:
        - security_protocol
        - sasl_mechanism
        - sasl_jaas_config
        properties:
          sasl_mechanism:
            type: string
            const: PLAIN
            title: SASL Mechanism
            description: The SASL mechanism used for client connections. This may
              be any mechanism for which a security provider is available.
          sasl_jaas_config:
            type: string
            title: SASL JAAS Config
            default: ''
            description: The JAAS login context parameters for SASL connections in
              the format used by JAAS configuration files.
            airbyte_secret: true
          security_protocol:
            type: string
            const: SASL_PLAINTEXT
      - title: SASL SSL
        required:
        - security_protocol
        - sasl_mechanism
        - sasl_jaas_config
        properties:
          sasl_mechanism:
            enum:
            - GSSAPI
            - OAUTHBEARER
            - SCRAM-SHA-256
            - SCRAM-SHA-512
            - PLAIN
            type: string
            title: SASL Mechanism
            default: GSSAPI
            description: The SASL mechanism used for client connections. This may
              be any mechanism for which a security provider is available.
          sasl_jaas_config:
            type: string
            title: SASL JAAS Config
            default: ''
            description: The JAAS login context parameters for SASL connections in
              the format used by JAAS configuration files.
            airbyte_secret: true
          security_protocol:
            type: string
            const: SASL_SSL
      title: Protocol
      description: The Protocol used to communicate with brokers.
    client_id:
      type: string
      title: Client ID
      examples:
      - airbyte-consumer
      description: An ID string to pass to the server when making requests. The purpose
        of this is to be able to track the source of requests beyond just ip/port
        by allowing a logical application name to be included in server-side request
        logging.
    test_topic:
      type: string
      title: Test Topic
      examples:
      - test.topic
      description: The Topic to test in case the Airbyte can consume messages.
    polling_time:
      type: integer
      title: Polling Time
      default: 100
      description: Amount of time Kafka connector should try to poll for messages.
    subscription:
      type: object
      oneOf:
      - title: Manually assign a list of partitions
        required:
        - subscription_type
        - topic_partitions
        properties:
          topic_partitions:
            type: string
            title: List of topic:partition Pairs
            examples:
            - sample.topic:0, sample.topic:1
          subscription_type:
            type: string
            const: assign
            description: 'Manually assign a list of partitions to this consumer. This
              interface does not allow for incremental assignment and will replace
              the previous assignment (if there is one).

              If the given list of topic partitions is empty, it is treated the same
              as unsubscribe().'
      - title: Subscribe to all topics matching specified pattern
        required:
        - subscription_type
        - topic_pattern
        properties:
          topic_pattern:
            type: string
            title: Topic Pattern
            examples:
            - sample.topic
          subscription_type:
            type: string
            const: subscribe
            description: The Topic pattern from which the records will be read.
      title: Subscription Method
      description: You can choose to manually assign a list of partitions, or subscribe
        to all topics matching specified pattern to get dynamically assigned partitions.
    MessageFormat:
      type: object
      oneOf:
      - title: JSON
        properties:
          deserialization_type:
            type: string
            const: JSON
      - title: AVRO
        properties:
          schema_registry_url:
            type: string
            examples:
            - http://localhost:8081
          deserialization_type:
            const: AVRO
          deserialization_strategy:
            enum:
            - TopicNameStrategy
            - RecordNameStrategy
            - TopicRecordNameStrategy
            type: string
            default: TopicNameStrategy
          schema_registry_password:
            type: string
            default: ''
          schema_registry_username:
            type: string
            default: ''
      title: MessageFormat
      description: 'The serialization used based on this '
    repeated_calls:
      type: integer
      title: Repeated Calls
      default: 3
      description: The number of repeated calls to poll() if no messages were received.
    max_poll_records:
      type: integer
      title: Max Poll Records
      default: 500
      description: The maximum number of records returned in a single call to poll().
        Note, that max_poll_records does not impact the underlying fetching behavior.
        The consumer will cache the records from each fetch request and returns them
        incrementally from each poll.
    retry_backoff_ms:
      type: integer
      title: Retry Backoff, ms
      default: 100
      description: The amount of time to wait before attempting to retry a failed
        request to a given topic partition. This avoids repeatedly sending requests
        in a tight loop under some failure scenarios.
    auto_offset_reset:
      enum:
      - latest
      - earliest
      - none
      type: string
      title: Auto Offset Reset
      default: latest
      description: 'What to do when there is no initial offset in Kafka or if the
        current offset does not exist any more on the server - earliest: automatically
        reset the offset to the earliest offset, latest: automatically reset the offset
        to the latest offset, none: throw exception to the consumer if no previous
        offset is found for the consumer''s group, anything else: throw exception
        to the consumer.'
    bootstrap_servers:
      type: string
      title: Bootstrap Servers
      examples:
      - kafka-broker1:9092,kafka-broker2:9092
      description: A list of host/port pairs to use for establishing the initial connection
        to the Kafka cluster. The client will make use of all servers irrespective
        of which servers are specified here for bootstrapping&mdash;this list only
        impacts the initial hosts used to discover the full set of servers. This list
        should be in the form <code>host1:port1,host2:port2,...</code>. Since these
        servers are just used for the initial connection to discover the full cluster
        membership (which may change dynamically), this list need not contain the
        full set of servers (you may want more than one, though, in case a server
        is down).
    client_dns_lookup:
      enum:
      - default
      - use_all_dns_ips
      - resolve_canonical_bootstrap_servers_only
      type: string
      title: Client DNS Lookup
      default: use_all_dns_ips
      description: Controls how the client uses DNS lookups. If set to use_all_dns_ips,
        connect to each returned IP address in sequence until a successful connection
        is established. After a disconnection, the next IP is used. Once all IPs have
        been used once, the client resolves the IP(s) from the hostname again. If
        set to resolve_canonical_bootstrap_servers_only, resolve each bootstrap address
        into a list of canonical names. After the bootstrap phase, this behaves the
        same as use_all_dns_ips. If set to default (deprecated), attempt to connect
        to the first IP address returned by the lookup, even if the lookup returns
        multiple IP addresses.
    enable_auto_commit:
      type: boolean
      title: Enable Auto Commit
      default: true
      description: If true, the consumer's offset will be periodically committed in
        the background.
    request_timeout_ms:
      type: integer
      title: Request Timeout, ms
      default: 30000
      description: The configuration controls the maximum amount of time the client
        will wait for the response of a request. If the response is not received before
        the timeout elapses the client will resend the request if necessary or fail
        the request if retries are exhausted.
    max_records_process:
      type: integer
      title: Maximum Records
      default: 100000
      description: The Maximum to be processed per execution
    receive_buffer_bytes:
      type: integer
      title: Receive Buffer, bytes
      default: 32768
      description: The size of the TCP receive buffer (SO_RCVBUF) to use when reading
        data. If the value is -1, the OS default will be used.
    auto_commit_interval_ms:
      type: integer
      title: Auto Commit Interval, ms
      default: 5000
      description: The frequency in milliseconds that the consumer offsets are auto-committed
        to Kafka if enable.auto.commit is set to true.
  additionalProperties: true
schemaType: {}
