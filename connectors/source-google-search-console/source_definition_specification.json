{
  "sourceDefinitionId": "eb4c9e00-db83-4d63-a386-39cfa91012a8",
  "documentationUrl": "https://docs.airbyte.com/integrations/sources/google-search-console",
  "connectionSpecification": {
    "type": "object",
    "title": "Google Search Console Spec",
    "$schema": "http://json-schema.org/draft-07/schema#",
    "required": [
      "site_urls",
      "authorization"
    ],
    "properties": {
      "end_date": {
        "type": "string",
        "order": 2,
        "title": "End Date",
        "format": "date",
        "pattern": "^$|^[0-9]{4}-[0-9]{2}-[0-9]{2}$",
        "examples": [
          "2021-12-12"
        ],
        "description": "UTC date in the format YYYY-MM-DD. Any data created after this date will not be replicated. Must be greater or equal to the start date field. Leaving this field blank will replicate all data from the start date onward.",
        "pattern_descriptor": "YYYY-MM-DD"
      },
      "site_urls": {
        "type": "array",
        "items": {
          "type": "string"
        },
        "order": 0,
        "title": "Website URL Property",
        "examples": [
          "https://example1.com/",
          "sc-domain:example2.com"
        ],
        "description": "The URLs of the website property attached to your GSC account. Learn more about properties <a href=\"https://support.google.com/webmasters/answer/34592?hl=en\">here</a>."
      },
      "data_state": {
        "enum": [
          "final",
          "all"
        ],
        "type": "string",
        "order": 6,
        "title": "Data Freshness",
        "default": "final",
        "examples": [
          "final",
          "all"
        ],
        "description": "If set to 'final', the returned data will include only finalized, stable data. If set to 'all', fresh data will be included. When using Incremental sync mode, we do not recommend setting this parameter to 'all' as it may cause data loss. More information can be found in our <a href='https://docs.airbyte.com/integrations/source/google-search-console'>full documentation</a>."
      },
      "start_date": {
        "type": "string",
        "order": 1,
        "title": "Start Date",
        "format": "date",
        "default": "2021-01-01",
        "pattern": "^[0-9]{4}-[0-9]{2}-[0-9]{2}$",
        "always_show": true,
        "description": "UTC date in the format YYYY-MM-DD. Any data before this date will not be replicated.",
        "pattern_descriptor": "YYYY-MM-DD"
      },
      "authorization": {
        "type": "object",
        "oneOf": [
          {
            "type": "object",
            "title": "OAuth",
            "required": [
              "auth_type",
              "client_id",
              "client_secret",
              "refresh_token"
            ],
            "properties": {
              "auth_type": {
                "type": "string",
                "const": "Client",
                "order": 0
              },
              "client_id": {
                "type": "string",
                "title": "Client ID",
                "description": "The client ID of your Google Search Console developer application. Read more <a href=\"https://developers.google.com/webmaster-tools/v1/how-tos/authorizing\">here</a>.",
                "airbyte_secret": true
              },
              "access_token": {
                "type": "string",
                "title": "Access Token",
                "description": "Access token for making authenticated requests. Read more <a href=\"https://developers.google.com/webmaster-tools/v1/how-tos/authorizing\">here</a>.",
                "airbyte_secret": true
              },
              "client_secret": {
                "type": "string",
                "title": "Client Secret",
                "description": "The client secret of your Google Search Console developer application. Read more <a href=\"https://developers.google.com/webmaster-tools/v1/how-tos/authorizing\">here</a>.",
                "airbyte_secret": true
              },
              "refresh_token": {
                "type": "string",
                "title": "Refresh Token",
                "description": "The token for obtaining a new access token. Read more <a href=\"https://developers.google.com/webmaster-tools/v1/how-tos/authorizing\">here</a>.",
                "airbyte_secret": true
              }
            }
          },
          {
            "type": "object",
            "title": "Service Account Key Authentication",
            "required": [
              "auth_type",
              "service_account_info",
              "email"
            ],
            "properties": {
              "email": {
                "type": "string",
                "title": "Admin Email",
                "description": "The email of the user which has permissions to access the Google Workspace Admin APIs."
              },
              "auth_type": {
                "type": "string",
                "const": "Service",
                "order": 0
              },
              "service_account_info": {
                "type": "string",
                "title": "Service Account JSON Key",
                "examples": [
                  "{ \"type\": \"service_account\", \"project_id\": YOUR_PROJECT_ID, \"private_key_id\": YOUR_PRIVATE_KEY, ... }"
                ],
                "description": "The JSON key of the service account to use for authorization. Read more <a href=\"https://cloud.google.com/iam/docs/creating-managing-service-account-keys\">here</a>.",
                "airbyte_secret": true
              }
            }
          }
        ],
        "order": 3,
        "title": "Authentication Type",
        "description": ""
      },
      "custom_reports": {
        "type": "string",
        "order": 4,
        "title": "Custom Reports",
        "description": "(DEPRCATED) A JSON array describing the custom reports you want to sync from Google Search Console. See our <a href='https://docs.airbyte.com/integrations/sources/google-search-console'>documentation</a> for more information on formulating custom reports.",
        "airbyte_hidden": true
      },
      "custom_reports_array": {
        "type": "array",
        "items": {
          "type": "object",
          "title": "Custom Report Config",
          "required": [
            "name",
            "dimensions"
          ],
          "properties": {
            "name": {
              "type": "string",
              "title": "Name",
              "description": "The name of the custom report, this name would be used as stream name"
            },
            "dimensions": {
              "type": "array",
              "items": {
                "enum": [
                  "country",
                  "date",
                  "device",
                  "page",
                  "query"
                ],
                "title": "ValidEnums",
                "description": "An enumeration of dimensions."
              },
              "title": "Dimensions",
              "default": [
                "date"
              ],
              "minItems": 0,
              "description": "A list of available dimensions. Please note, that for technical reasons `date` is the default dimension which will be included in your query whether you specify it or not. Primary key will consist of your custom dimensions and the default dimension along with `site_url` and `search_type`."
            }
          }
        },
        "order": 5,
        "title": "Custom Reports",
        "description": "You can add your Custom Analytics report by creating one."
      }
    }
  },
  "advancedAuth": {
    "authFlowType": "oauth2.0",
    "predicateKey": [
      "authorization",
      "auth_type"
    ],
    "predicateValue": "Client",
    "oauthConfigSpecification": {
      "completeOAuthOutputSpecification": {
        "type": "object",
        "properties": {
          "access_token": {
            "type": "string",
            "path_in_connector_config": [
              "authorization",
              "access_token"
            ]
          },
          "refresh_token": {
            "type": "string",
            "path_in_connector_config": [
              "authorization",
              "refresh_token"
            ]
          }
        }
      },
      "completeOAuthServerInputSpecification": {
        "type": "object",
        "properties": {
          "client_id": {
            "type": "string"
          },
          "client_secret": {
            "type": "string"
          }
        }
      },
      "completeOAuthServerOutputSpecification": {
        "type": "object",
        "properties": {
          "client_id": {
            "type": "string",
            "path_in_connector_config": [
              "authorization",
              "client_id"
            ]
          },
          "client_secret": {
            "type": "string",
            "path_in_connector_config": [
              "authorization",
              "client_secret"
            ]
          }
        }
      }
    }
  },
  "jobInfo": {
    "id": "dda3bb89-78ff-4b7b-b041-23c01becfbe0",
    "configType": "get_spec",
    "configId": "Optional.empty",
    "createdAt": 1705523877603,
    "endedAt": 1705523877603,
    "succeeded": true,
    "connectorConfigurationUpdated": false,
    "logs": {
      "logLines": []
    }
  }
}